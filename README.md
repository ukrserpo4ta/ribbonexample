### What is this repository for? ###
This is a simple demo of Ribbon client-side balancing.

Project contains:
1.Eureka Server("eurekaserver");
2.Service ("service");
3.Client with ribbon client-side balancing ("clientwithribbon").

### How to run and test? ###

1. Start "Eureka Server"  - just run EurekaApp.class in "eurekaserver" module, open http://localhost:9000 in your browser;

2. Run "Service" replicas. How to do it easy in Windows:
    - go to "/service" folder and execute "mvn install" comand in "cmd.exe" or do it in your IDE for this module;
	- after "build success" you should see "/target" folder with "eurekaservice-1.0-SNAPSHOT.jar";
	- copy all files from "/cmdFilesForRunReplicas" folder to "/target";
	- run all of this files from "target" folder.
	You have created 5 service instances, open in your browser (http://localhost:8001, http://localhost:8002, http://localhost:8003, http://localhost:8004, http://localhost:8005).
	And you should see that these instances currently registered with Eureka at http://localhost:9000

3. Run "Client" with ribbon client-side balancing - just run RunApp.class in "clientwithribbon" module, open http://localhost:5555 in your browser.
   To see how it works reload this page(http://localhost:5555) few times.
