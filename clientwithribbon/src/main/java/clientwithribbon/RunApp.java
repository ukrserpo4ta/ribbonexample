package clientwithribbon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import clientwithribbon.config.RibbonConfiguration;

@RibbonClient (name = "ping-a-server", configuration = RibbonConfiguration.class)
@EnableEurekaClient
@SpringBootApplication
public class RunApp {

  public static void main(String[] args) {
    SpringApplication.run(RunApp.class, args);
  }
}
